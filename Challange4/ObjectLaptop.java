package Challange4;

public class ObjectLaptop {
    String namaLaptop;
    String merkLaptop;
    String processorLaptop;
    String warnaLaptop;
    int tahunLaptop;

        public ObjectLaptop(String nama, String merk, String processor, String warna, int tahun) {
            namaLaptop = nama;
            merkLaptop = merk;
            processorLaptop = processor;
            warnaLaptop = warna;
            tahunLaptop = tahun;
        }

        public void getOn() {
            System.out.println("pencet tombol power");
        }
        public void getOff() {
            System.out.println("Cara Mematikan laptop");
            System.out.println("klik shutdown");
        }
        public void updateOs() {
            System.out.println("klik updateOs");
        }
        
}


/*
static void getOn() {
    System.out.println("pencet tombol power");
}
static void getOff() {
    System.out.println("klik shutdown");
}
static void updateOs() {
    System.out.println("klik updateOs");
}
 */

