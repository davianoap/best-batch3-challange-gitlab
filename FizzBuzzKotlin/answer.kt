fun main() {
    println("Masukkan Angka yang diinginkan : ")
    val input = readln().toInt()
    for (i in 1.. input) {
        if(i %3 == 0 && i %5 == 0) {
            println("FizzBuzz")
        } else if (i%3 == 0) {
            println("Fizz")
        } else if (i%5 == 0) {
            println("Buzz")
        } else {
            println(i)
        }
    }

}
