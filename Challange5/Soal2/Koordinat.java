package Challange5.Soal2;

public class Koordinat {
    private int koorX;
    private char koorY;

    public Koordinat(int koorX, char koorY) {
        this.koorX = koorX;
        this.koorY = koorY;
    }
    
    public void move (int i,int gerak){
        if(i==1){
            if(koorY-gerak < 'A')
                koorY = 'A';
            else
                koorY-=gerak;
        }else if(i==2){
            if(koorX-gerak < 1 )
                koorX = 1;
            else
                koorX-=gerak;
        }else if(i==3){
            if(koorX+gerak > 10 )
                koorX = 10;
            else
                koorX+=gerak;
        }else{
            if(koorY+gerak > 'J')
                koorY = 'J';
            else
                koorY+=gerak; 
        }
    }
    
    public void PullOver (int i){
        if(i==1){
            koorY = 'A';
        }else if(i==2){
            koorX = 1;
        }else if(i==3){
            koorX = 10;
        }else
            koorY = 'J';
                
    }
    
    public void restart(){
        koorX = 5;
        koorY = 'F';
    }
    
    public String showKoordinat(){
        return koorY + Integer.toString(koorX);
    }
}