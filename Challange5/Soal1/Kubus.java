package Challange5.Soal1;

public class Kubus {
    double sisi;

    public Kubus(double sisi) {
        this.sisi = sisi;
    }

    public void getVolume() {
        double volume;
        volume = sisi*sisi*sisi;

        System.out.println("Volume Kubus = " + volume);
    }
}
