package Challange5.Soal1;

public class Segitiga {
    double tinggi;
    double alas;

    public Segitiga(double tinggi,double alas) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    public void getLuas() {
        double luas;
        luas = alas*tinggi*0.5;

        System.out.println("Luas segitiga = "+luas);
    }
}
