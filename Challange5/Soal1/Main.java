package Challange5.Soal1;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int inputPilihan;

        do {

            System.out.println("Kalkulator Sederhana");
            System.out.println("Pilih \n1.Luas Segitiga\n2.Volume Balok\n3.Volumekubus");
            Scanner sc = new Scanner(System.in);
            inputPilihan = sc.nextInt();

            switch(inputPilihan) {
                case 1:
                System.out.println("Kalkulator Segitiga");
                System.out.println("Masukkan Alas : ");
                int alas = sc.nextInt();
                System.out.println("Masukkan Tinggi : ");
                int tinggi = sc.nextInt();
                Segitiga segitiga = new Segitiga(alas, tinggi);
                segitiga.getLuas();
                break;

                case 2:
                System.out.println("Kalkulator Balok");
                System.out.println("Masukkan Panjang : ");
                int p = sc.nextInt();
                System.out.println("Masukkan Lebar : ");
                int l = sc.nextInt();
                System.out.println("Masukkan Tinggi : ");
                int t = sc.nextInt();
                Balok balok = new Balok(p, l,t);
                balok.getVolume();
                break;

                case 3:
                System.out.println("Kalkulator Kubus");
                System.out.println("Masukkan Sisi: ");
                int sisi = sc.nextInt();
                Kubus kubus = new Kubus(sisi);
                kubus.getVolume();
                break;
            }
        }while(inputPilihan!=0);    
    }
}
