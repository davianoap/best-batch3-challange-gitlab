package com.daviano.demospring.service

import com.daviano.demospring.entity.Book
import com.daviano.demospring.repository.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
@Service
class BookService @Autowired constructor(
    private val bookRepository: BookRepository
) {
    fun getBooks(): List<Book> {
        return bookRepository.findAll()
    }

    fun addNewBook(book: Book): Book {
        return bookRepository.save(book)
    }

    fun updateBook(id: Int, book:Book): Book {
        val updatedBook = bookRepository.findById(id).get()
        updatedBook.title = book.title
        updatedBook.isbn = book.isbn
        updatedBook.author = book.author
        updatedBook.year = book.year

        return bookRepository.save(updatedBook)
    }

    fun deleteBook(id: Int): Int {
        bookRepository.deleteById(id)
        return id
    }
}

