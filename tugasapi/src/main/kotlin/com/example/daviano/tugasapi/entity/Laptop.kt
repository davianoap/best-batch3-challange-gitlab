package com.example.daviano.tugasapi.entity

import javax.persistence.*

@Entity
@Table(name = "laptop")
data class Laptop(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,

    @Column(name = "nama")
    var nama: String? = null,

    @Column(name = "merk")
    var merk: String? = null,

    @Column(name = "processor")
    var processor: String? = null,

    @Column(name = "warna")
    var warna: String? = null,

    @Column(name = "tahun")
    var tahun: Int? = null

)