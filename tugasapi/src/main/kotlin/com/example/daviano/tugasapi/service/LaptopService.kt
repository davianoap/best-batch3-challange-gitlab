package com.example.daviano.tugasapi.service

import com.example.daviano.tugasapi.entity.Laptop
import com.example.daviano.tugasapi.repository.LaptopRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LaptopService @Autowired constructor(
    private val laptopRepository: LaptopRepository
) {
    fun getLaptop(): List<Laptop>{
        return laptopRepository.findAll()
    }

    fun addNewLaptop(laptop: Laptop): Laptop {
        return laptopRepository.save(laptop)
    }

    fun updateLaptop(id: Int, laptop:Laptop): Laptop {
        val updatedLaptop = laptopRepository.findById(id).get()
        updatedLaptop.nama = laptop.nama
        updatedLaptop.merk = laptop.merk
        updatedLaptop.processor = laptop.processor
        updatedLaptop.warna = laptop.warna
        updatedLaptop.tahun = laptop.tahun

        return laptopRepository.save(updatedLaptop)
    }

    fun deleteLaptop(id: Int): Int {
        laptopRepository.deleteById(id)
        return id
    }
}