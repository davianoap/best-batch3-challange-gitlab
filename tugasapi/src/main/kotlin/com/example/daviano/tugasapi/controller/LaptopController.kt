package com.example.daviano.tugasapi.controller

import com.example.daviano.tugasapi.entity.Laptop
import com.example.daviano.tugasapi.service.LaptopService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Controller
@RequestMapping(value = ["/laptop"], produces = [MediaType.APPLICATION_JSON_VALUE])
class LaptopController @Autowired constructor(
    private val laptopService: LaptopService
) {
    @GetMapping
    fun getAllLaptop(): ResponseEntity<List<Laptop>> {
        return ResponseEntity(laptopService.getLaptop(),HttpStatus.OK)
    }

    @PostMapping
    fun addNewLaptop(
        @RequestBody laptop: Laptop
    ): ResponseEntity<Laptop> {
        return ResponseEntity(laptopService.addNewLaptop(laptop),HttpStatus.OK)
    }

    @PutMapping("{id}")
    fun updateLaptop(
        @PathVariable("id") id: Int,
        @RequestBody laptop: Laptop
    ): ResponseEntity<Laptop> {
        return ResponseEntity(laptopService.updateLaptop(id,laptop),HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    fun deleteLaptop (
        @PathVariable("id")id: Int
    ): ResponseEntity<Int> {
        return ResponseEntity(laptopService.deleteLaptop(id),HttpStatus.OK)
    }
}
