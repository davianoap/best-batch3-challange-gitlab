package com.example.daviano.tugasapi.repository

import com.example.daviano.tugasapi.entity.Laptop
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LaptopRepository: JpaRepository<Laptop, Int>
