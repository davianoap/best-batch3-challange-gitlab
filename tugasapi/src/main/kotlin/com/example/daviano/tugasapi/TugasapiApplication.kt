package com.example.daviano.tugasapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TugasapiApplication

fun main(args: Array<String>) {
	runApplication<TugasapiApplication>(*args)
}
