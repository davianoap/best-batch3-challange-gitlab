package service

import java.util.StringJoiner

interface LoginService {
    fun login(username:String, password:String): Boolean
}