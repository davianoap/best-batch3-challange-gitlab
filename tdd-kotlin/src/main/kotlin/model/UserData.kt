package model

data class UserData(
    val id: Int,
    val email: String,
    val username: String,
    val password: String,
    val address: String? = null
)