import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import service.LoginService
import service.LoginServiceImpl

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
 class LoginTest {

    private lateinit var loginService: LoginService
    @BeforeAll
    fun setupPerTest() {
        loginService = LoginServiceImpl()
    }
    @Test
    fun usernameIsEmail() {
        val username = "daviano@investree.id"
        val password = "bebas"

        val result = loginService.login(username,password)

        Assertions.assertTrue(result)
    }

    @Test
    fun usernameIsNotEmail() {
        val username = "daviano"
        val password = "bebas"

        val result = loginService.login(username,password)
        Assertions.assertFalse(result)
    }

    @Test
    fun usernameMustNotBeEmpty() {
        val username = ""
        val password = "bebas"

        val result = loginService.login(username,password)

        Assertions.assertFalse(result)
    }

    @Test
    fun passwordMustNotBeEmpty () {
        val usernameMock = "daviano@investree.id"
        val password = ""

        val result = loginService.login(usernameMock, password)
        Assertions.assertFalse(result)
    }
}